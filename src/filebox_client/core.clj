(ns filebox_client.core
  (:require [clojure.tools.cli :as cli]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [filebox_client.utils :as utils]
            [filebox_client.boxapi :as api]
            [filebox_client.db :as db]
            [filebox_client.git :as git])
  (:import [java.nio.file Files Path Paths
            SimpleFileVisitor FileVisitResult]))

(def conf {:host "andaman.local"
           :port 8080
           :user "gopher"
           :password "gopher1"
           :client-id "vaio_linux"
           :local-db-conf
             {:local-db-file "c:/Temp/test.db"}
           :remote-db-conf
             {:remote-host "andaman.local"
              :remote-port 8080
              :username "gopher"
              :password "gopher1"
              :local-dir "C:/Temp/test-dir"}})

;; parses command arguments
(defn parse-args [args]
  (let [[options a banner] (cli/cli args
           ["-h" "--help" "Show help" :default false :flag true]
           ["-l" "--list" "list specified type of directories {synced,unsynced,all}"]
           ["-a" "--add" "add local directory for sync"]
           ["-p" "--pull" "pull remote directory content for sync"]
           ["-s" "--sync" "sync the given dir which is added or pulled"])]
    (when (:help options)
      (println banner)
      (comment System/exit 0))
    options))

(def SEP #",")
(def CLIENT_LIST_KEY "CLIENT_LIST")
(def LOC_LIST_KEY_PREFIX "LOC_LIST")
(def LOC_PREFIX "LOC")

(defn- get-clients [db-handle]
  (let [clients (db/get db-handle CLIENT_LIST_KEY)]
    (if (empty? clients) '()
      (string/split clients SEP))))

(defn- get-locations [db-handle client-id]
  (let [locs (db/get db-handle (str LOC_LIST_KEY_PREFIX "." client-id))]
    (if (empty? locs) '()
      (doall
       (for [loc-id (string/split locs SEP)]
        (db/get db-handle (str LOC_PREFIX "." loc-id)))))))

;; map containing currently synced local and remote folders
;; in this client
(defn- list-synced-dir [conf]
  (let [h (db/open-db (:local-db-file (:local-db-conf conf)))]
    (try
       (into {} (for [client-id (get-clients h)]
                  {client-id (get-locations h client-id)}))
      (finally
       (db/close-db h)))))

;; map containing all available client-id and thier respective top level
;; synced folders
(defn- list-remote-dir [conf]
  (let [remote-db-conf (:remote-db-conf conf)]
    (do
      (comment (git/sync-repo remote-db-conf))
      (let [local-dir (io/file (:local-dir remote-db-conf))
            clients (filter
                     #(not (.startsWith (.getName %) "."))
                     (.listFiles local-dir))]
        (into {} (for [client clients]
                   {(.getName client)
                    (map #(.getName %) (.listFiles client))}))))))

;; list the content at the given type
(defn list-dir [conf dir-type]
  (let [all-dir (list-remote-dir conf)
        synced-dir (list-synced-dir conf)]
    (cond
     (= dir-type "synced")
       synced-dir
     (= dir-type "unsynced")
       (into {} (for [[c al] all-dir
                      :let [sl (get synced-dir c)
                            func (if (empty? sl)
                                   identity
                                   #(= (.indexOf sl %) -1))]]
                  {c (filter func al)}))
     (= dir-type "all") all-dir)))

(defn- print-list [file-list]
  (doseq [[c dl] file-list]
      (println (str c ":"))
      (doseq [d dl]
        (println (str "\t" d)))))

;; add direcory for syncing on filebox server
(defn add-dir [conf local-dir]
  (let [visitor
        (proxy [SimpleFileVisitor] []
          (visitFile
           [file _] (do
                      (api/sendfile conf (str local-dir) (str file))
                      (FileVisitResult/CONTINUE))))]
    (Files/walkFileTree (utils/to-path [local-dir]) visitor)))

;; pull remote directory on local disk
(defn pull-dir [conf remote-dir]
  (println "pull dir is not supported yet"))

;; sync content of the directory with filebox server
(defn sync-dir [conf sync-dir]
  (println "sync dir is not supported yet"))

(defn -main [args]
  (let [commands (parse-args args)]
    (println commands)
     (if (:list commands)
       (print-list (list-dir conf (:pull commands))))
     (if (:add commands) (add-dir conf (:add commands)))
     (if (:pull commands) (pull-dir conf (:pull commands)))
     (if (:sync commands) (sync-dir conf (:sync commands)))))

(print-list (list-dir conf "all"))
