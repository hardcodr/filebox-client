(ns filebox_client.db
  (:require [filebox_client.utils :as utils]
            [clojure.java.io :as io])
  (:import [org.iq80.leveldb Options DB]
           [org.iq80.leveldb.impl Iq80DBFactory]))

;; converts string to byte array
(defn- s2b [s] (Iq80DBFactory/bytes s))

;; converts byte array to string
(defn- b2s [b] (Iq80DBFactory/asString b))

;; open existing db or create new db and return the db handle
(defn open-db [path]
  (let [options (Options.)]
    (do
      (.createIfMissing options true)
      (let [factory (Iq80DBFactory/factory)]
        (.open factory (io/file path) options)))))

;; close the db handle
(defn close-db [db-handle]
  (.close db-handle))

;; put key-value pair in db
(defn put [db-handle k v]
  (.put db-handle (s2b k) (s2b v)))

;; put entire map containing key-value pair into the db
;; keyword as key also supported
(defn put-all [db-handle dict]
  (let [batch (.createWriteBatch db-handle)]
    (try
      (do
        (doseq [[k v] dict]
          (let [kp (if (keyword? k) (name k) k)]
            (.put batch  (s2b kp) (s2b v))))
        (.write db-handle batch))
      (finally
       (.close batch)))))

;; get value for given key from db
(defn get [db-handle k]
  (b2s (.get db-handle (s2b k))))