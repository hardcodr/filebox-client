(ns filebox_client.git
  (:require [filebox_client.utils :as utils]
            [clojure.java.io :as io])
  (:import [org.eclipse.jgit.api Git]
           [org.eclipse.jgit.transport UsernamePasswordCredentialsProvider]))

(def conf
  {:remote-host "andaman.local"
   :remote-port 8080
   :username "gopher"
   :password "gopher1"
   :local-dir "C:/Temp/test-dir"})

(defn- create-repo-url
  ([host] (create-repo-url host 80))
  ([host port] (utils/create-http-url host port "filebox/gitrepo.git")))

(defn- clone-git-repo [url cp local-dir]
  (let [clone-cmd (Git/cloneRepository)]
   (->
     (.setURI clone-cmd url)
     (.setCredentialsProvider cp)
     (.setDirectory local-dir)
     (.call))
    true))

(defn- sync-git-repo [cp local-dir]
  (let [pull-cmd (.pull (Git/open local-dir))]
    (->
     (.setRebase pull-cmd true)
     (.setCredentialsProvider cp)
     (.call)
     (.isSuccessful))))

;; sync remote repo to the local directory
;; if local directory is not present then clone otherwise
;; just pull the remote repo content
(defn sync-repo [conf]
  (let [local-dir (io/file (:local-dir conf))
        url (create-repo-url
             (:remote-host conf)(:remote-port conf))
        cp (UsernamePasswordCredentialsProvider.
            (:username conf) (:password conf))]
      (if (.exists local-dir)
        (sync-git-repo cp local-dir)
        (clone-git-repo url cp local-dir))))
