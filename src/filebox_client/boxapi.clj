(ns filebox_client.boxapi
  (:require [clj-http.client :as http]
            [filebox_client.utils :as utils]
            [clojure.java.io :as io]))

(def STATUS_OK 200)

;; returns url where files can be uploaded
(defn- upload-url
  ([host] (upload-url host 80))
  ([host port] (utils/create-http-url host port "filebox/upload")))

;; returns url where resource can be retrieved or removed
(defn- resource-url
  ([host] (resource-url host 80))
  ([host port] (utils/create-http-url host port "/filebox/handle")))

;; returns map containing relative path (execluding filename) from
;; given root-dir and name of file
(defn- relpath-&-filename [root-dir file]
  (let [tpath (.toFile
               (.relativize
                (utils/to-path [root-dir])
                (utils/to-path [file])))]
      {:relpath (utils/parent tpath)
       :filename (utils/filename tpath)}))

;; send this file to the filebox server
(defn sendfile [conf root-dir file]
  (let [url (upload-url (:host conf) (:port conf))
        auth (str (:user conf) ":" (:password conf))
        client-id (:client-id conf)
        path-&-name (relpath-&-filename root-dir file)
        multipart-params [["relpath" (str client-id "/" (:relpath path-&-name))]
                          ["filename" (:filename path-&-name)]
                          ["version" "0"] ;versioning is not supported
                          ["creator" client-id]
                          ["creation-ts" (utils/current-ts)]
                          ["is_encrypted" "no"] ;so does encryption
                          ["tempfile" (io/file (str file))]]
        result (http/post url {:basic-auth auth
                               :throw-exceptions false
                               :multipart multipart-params})]
    (if (= (:status result) STATUS_OK)
      true
      false)))

;; get given file from filebox server
(defn getfile [conf client-id rel-filename outfile]
  (let [url (resource-url (:host conf) (:port conf))
        auth (str (:user conf) ":" (:password conf))
        file (io/file rel-filename)
        query-params {:relpath (str client-id "/" (utils/parent file))
                      :filename (utils/filename file)}
        result (http/get url {:basic-auth auth
                              :throw-exceptions false
                              :follow-redirects true
                              :query-params query-params
                              :as :stream})]
    (if (= (:status result) STATUS_OK)
      (do
        (io/copy
         (:body result)
         (io/file outfile))
        true)
      false)))

;; delete file from filebox server
(defn delfile [conf client-id rel-filename is-hard?]
  (let [url (resource-url (:host conf) (:port conf))
        auth (str (:user conf) ":" (:password conf))
        file (io/file rel-filename)
        query-params {:relpath
                      (str client-id "/" (utils/parent file))
                      :filename (.getName file)
                      :type (if is-hard? "hard" "soft")}
        result (http/delete url {:basic-auth auth
                              :throw-exceptions false
                              :follow-redirects true
                              :query-params query-params
                              :as :stream})]
    (if (= (:status result) STATUS_OK)
      true false)))
