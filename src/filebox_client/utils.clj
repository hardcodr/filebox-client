(ns filebox_client.utils
  (:import [java.nio.file Paths]
           [java.util Date]
           [org.apache.commons.io FilenameUtils]))

;; converts arrays of string to a java.nio.file.Path object
(defn to-path [path]
  (Paths/get (first path)
             (into-array String (rest path))))

(defn parent [file]
  (if (nil? (.getParent file))
    (str) ;if no parent then return empty string
    (-> (.getParent file) ;else unix style path
        (FilenameUtils/separatorsToUnix))))

(defn filename [file]
  (.getName file))

(defn current-ts []
  (str (.getTime (Date.))))

(defn create-http-url [host port path]
  (if (= port 80)
    (str "http://" host "/" path)
    (str "http://" host ":" port "/" path)))