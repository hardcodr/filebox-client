(defproject filebox_client "0.0.1-SNAPSHOT"
  :description "boxsync - Filebox Client"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/tools.cli "0.2.2"]
                 [commons-io/commons-io "2.4"]
                 [org.eclipse.jgit/org.eclipse.jgit "2.3.1.201302201838-r"]
                 [org.iq80.leveldb/leveldb "0.5"]]
  :java-cmd "C:/Development/Tools/jdk1.7.0_09/bin/java")
